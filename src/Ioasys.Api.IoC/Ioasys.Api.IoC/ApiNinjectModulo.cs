﻿using Ioasys.Api.Application;
using Ioasys.Api.Application.Interfaces;
using Ioasys.Api.Data.Repositories;
using Ioasys.Api.Domain.Interfaces.Repository;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.IoC
{
    public class ApiNinjectModulo : NinjectModule
    {
        public override void Load()
        {
            //Domain
            Bind<IEnterpriseRepository>().To<EnterpriseRepository>();
            Bind<IInvestorRepository>().To<InvestorRepository>();

            //AppService
            Bind<IEnterpriseAppService>().To<EnterpriseAppService>();
            Bind<IInvestorAppService>().To<InvestorAppService>();
        }
    }
}
