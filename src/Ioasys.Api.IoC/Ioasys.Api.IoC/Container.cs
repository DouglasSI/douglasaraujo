﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.IoC
{
    public class Container
    {
        public StandardKernel GetModule()
        {
            return new StandardKernel(new ApiNinjectModulo());
        }
    }
}
