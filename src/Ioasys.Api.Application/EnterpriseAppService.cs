﻿using AutoMapper;
using Ioasys.Api.Application.Interfaces;
using Ioasys.Api.Data.Repositories;
using System;
using System.Collections.Generic;

namespace Ioasys.Api.Application
{
    public class EnterpriseAppService : IEnterpriseAppService
    {
        private readonly EnterpriseRepository _enterpriseRepository = new EnterpriseRepository();

        public ViewModels.enterprises getByTypeName(int id, string name)
        {
            return Mapper.Map<Ioasys.Api.Domain.Entities.enterprises, ViewModels.enterprises>(_enterpriseRepository.getByTypeName(id, name));
        }

        public ViewModels.enterprise getById(int id)
        {
            
            return Mapper.Map<Ioasys.Api.Domain.Entities.enterprises, ViewModels.enterprise>(_enterpriseRepository.getById(id));
        }

        public IEnumerable<ViewModels.enterprise> getAll()
        {

            return Mapper.Map<IEnumerable<Ioasys.Api.Domain.Entities.enterprises>, IEnumerable<ViewModels.enterprise>>(_enterpriseRepository.teste());
        }

        public void Dispose()
        {
            _enterpriseRepository.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
