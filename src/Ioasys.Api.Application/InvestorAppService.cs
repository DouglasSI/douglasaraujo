﻿using AutoMapper;
using Ioasys.Api.Application.Interfaces;
using Ioasys.Api.Data.Repositories;
using System;


namespace Ioasys.Api.Application
{
    public class InvestorAppService : IInvestorAppService
    {
        private readonly InvestorRepository _investorRepository = new InvestorRepository();

        public ViewModels.investor getByUsernamePassword(string username, string password)
        {
            return Mapper.Map<Ioasys.Api.Domain.Entities.investor, ViewModels.investor>(_investorRepository.getByUsernamePassword(username, password));
        }

        public void Dispose()
        {
            _investorRepository.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
