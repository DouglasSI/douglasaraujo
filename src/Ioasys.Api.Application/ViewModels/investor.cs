﻿using Ioasys.Api.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Application.ViewModels
{
    public class investor
    {
        public int id { get; set; }
        public string investor_name { get; set; }
        public string email { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public double balance { get; set; }
        public string photo { get; set; }
        public double portfolio_value { get; set; }
        public bool first_acess { get; set; }
        public bool super_angel { get; set; }
        public portifolio portfolio { get; set; }
        public enterprises enterprise { get; set; }
    }
}
