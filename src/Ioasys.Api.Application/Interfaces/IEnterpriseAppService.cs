﻿using Ioasys.Api.Domain.Entities;
using System;
using System.Collections.Generic;

namespace Ioasys.Api.Application.Interfaces
{
    public interface IEnterpriseAppService : IDisposable
    {
        ViewModels.enterprises getByTypeName(int id, string name);

        ViewModels.enterprise getById(int id);

        IEnumerable<ViewModels.enterprise> getAll();
    }
}
