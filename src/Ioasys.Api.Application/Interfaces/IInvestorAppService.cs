﻿using Ioasys.Api.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Application.Interfaces
{
    public interface IInvestorAppService : IDisposable
    {
        Ioasys.Api.Application.ViewModels.investor getByUsernamePassword(string username, string password);
    }
}
