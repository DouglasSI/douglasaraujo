﻿using AutoMapper;
using Ioasys.Api.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Application.AutoMapper
{
    public class ToDomain : Profile
    {
        public override string ProfileName
        {
            get { return "ToDomain"; }
        }

        protected override void Configure()
        {
            Mapper.CreateMap<ViewModels.investor, investor>();
            Mapper.CreateMap<ViewModels.enterprise, enterprises>();
            Mapper.CreateMap<ViewModels.enterprises, enterprises>();
        }
    }

}
