﻿using Ioasys.Api.Application;
using Ioasys.Api.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ioasys.Api.Start.Controllers
{
    [RoutePrefix("api/v1")]
    [Route("{action=Index}")]
    public class ApiController : Controller
    {
        private readonly EnterpriseAppService _enterpriseAppService = new EnterpriseAppService();
        private readonly InvestorAppService _investor = new InvestorAppService();

        public ActionResult Index()
        {
            return View();
        }

        [Route("enterprises")]
        public JsonResult getAllEnterprises()
        {
            return Json(_enterpriseAppService.getAll());
        }

        [Route("enterprises/{id:int}")]
        public JsonResult getById(int id)
        {
            return Json(_enterpriseAppService.getById(id));
        }

        [Route("enterprises/{enterprise_types:int}{name:string}")]
        public JsonResult getWithFilter(int type, string name)
        {
            return Json(_enterpriseAppService.getByTypeName(type, name));
        }

        [Route("users/auth/sign_in")]
        public JsonResult login(string username, string password)
        {
            return Json(_investor.getByUsernamePassword(username, password));
        }

    }
}