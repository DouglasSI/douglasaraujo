﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Domain.Interfaces.Repository
{
    public interface IRepository<TEntity> : IDisposable where TEntity : class
    {
        IEnumerable<TEntity> getAll();

        TEntity getById(int id);

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicateOne, Expression<Func<TEntity, bool>> predicateTwo);
    }
}
