﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Domain.Entities
{
    public class portifolio
    {
        public portifolio()
        {
            enterprises = new List<enterprises>();
        }

        [Key]
        public int portifolio_id { get; set; }
        public int enterprises_number { get; set; }
        public virtual ICollection<enterprises> enterprises { get; set; }
        public int investor_id { get; set; }
        public investor investor { get; set; }
        public enterprises enterprises_data;
    }
}
