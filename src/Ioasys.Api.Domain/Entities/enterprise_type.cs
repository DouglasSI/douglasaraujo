﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Domain.Entities
{
    public class enterprise_type
    {
        [Key]
        public int id { get; set; }
        public string enterprise_type_name { get; set; }
        public enterprises enterprises{ get; set; }
    }
}
