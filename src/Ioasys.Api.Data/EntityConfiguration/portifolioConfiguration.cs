﻿using Ioasys.Api.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Data.EntityConfiguration
{
    public class portifolioConfiguration : EntityTypeConfiguration<portifolio>
    {
        public portifolioConfiguration()
        {
            HasKey(c => c.portifolio_id);

            HasOptional(c => c.investor).WithRequired(c => c.portfolio);

            ToTable("portifolio");
        }
    }
}
