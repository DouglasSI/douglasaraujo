﻿using Ioasys.Api.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Data.EntityConfiguration
{
    public class investorConfiguration : EntityTypeConfiguration<investor>
    {
        public investorConfiguration()
        {
            HasKey(c => c.id);

            Property(c => c.investor_name).HasMaxLength(150).IsRequired();
            Property(c => c.email).HasMaxLength(100).IsRequired();
            Property(c => c.password).HasMaxLength(100).IsRequired();

            ToTable("investors");
        }
    }
}
