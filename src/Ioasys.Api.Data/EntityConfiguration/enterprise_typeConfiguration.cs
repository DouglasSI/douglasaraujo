﻿using Ioasys.Api.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Data.EntityConfiguration
{
    public class enterprise_typeConfiguration : EntityTypeConfiguration<enterprise_type>
    {
        public enterprise_typeConfiguration()
        {
            HasKey(c => c.id);

            HasOptional(c => c.enterprises).WithRequired(c => c.enterprise_type).Map(t => t.MapKey("enterprise_type_id"));

            ToTable("enterprise_type");
        }
    }
}
