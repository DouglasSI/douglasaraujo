﻿using Ioasys.Api.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Data.EntityConfiguration
{
    public class enterprisesConfiguration : EntityTypeConfiguration<enterprises>
    {
        public enterprisesConfiguration()
        {
            HasKey(c => c.id);

            HasRequired(c => c.portifolio).WithMany(d => d.enterprises).HasForeignKey(e => e.portifolio_id);

            ToTable("enterprises");
        }
    }
}
