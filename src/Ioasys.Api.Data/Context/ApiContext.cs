﻿using Ioasys.Api.Data.EntityConfiguration;
using Ioasys.Api.Domain.Entities;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Ioasys.Api.Data.Context
{
    public class ApiContext : DbContext
    {
        public ApiContext() : base("DefaultConnection")
        {

        }

        public DbSet<investor> investor { get; set; }
        public DbSet<portifolio> portfolio { get; set; }
        public DbSet<enterprises> enterprises { get; set; }
        public DbSet<enterprise_type> enterprise_type { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Properties<string>().Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>().Configure(p => p.IsOptional());

            modelBuilder.Configurations.Add(new investorConfiguration());
            modelBuilder.Configurations.Add(new portifolioConfiguration());
            modelBuilder.Configurations.Add(new enterprisesConfiguration());
            modelBuilder.Configurations.Add(new enterprise_typeConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
