﻿using Ioasys.Api.Domain.Entities;
using Ioasys.Api.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Data.Repositories
{
    public class InvestorRepository : Repository<investor>, IInvestorRepository
    {
        public investor getByUsernamePassword(string username, string password)
        {
            return Find(c => c.investor_name == username, d => d.password == password).FirstOrDefault();
        }
    }
}
