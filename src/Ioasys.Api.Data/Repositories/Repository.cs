﻿using Ioasys.Api.Data.Context;
using Ioasys.Api.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected ApiContext Db;
        protected DbSet<TEntity> DbSet;

        public Repository()
        {
            Db = new ApiContext();
            DbSet = Db.Set<TEntity>();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicateOne, Expression<Func<TEntity, bool>> predicateTwo)
        {
            return DbSet.Where(predicateOne).Where(predicateTwo);
        }

        public virtual IEnumerable<TEntity> getAll()
        {
            return DbSet.ToList();
        }

        public virtual TEntity getById(int id)
        {
            using (var db = new ApiContext())
            {
                db.Configuration.LazyLoadingEnabled = false;
                return db.Set<TEntity>().Find(id);
            }
        }

        public void Dispose()
        {
            Db.Dispose();
            GC.SuppressFinalize(this);          
        }
    }
}
