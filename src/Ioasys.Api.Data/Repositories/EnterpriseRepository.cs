﻿using Ioasys.Api.Domain.Entities;
using Ioasys.Api.Domain.Interfaces.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ioasys.Api.Data.Repositories
{
    public class EnterpriseRepository : Repository<enterprises>, IEnterpriseRepository
    {
        public enterprises getByTypeName(int id, string name)
        {
            return Find(c => c.enterprise_type.id == id, d => d.enterprise_name.Contains(name)).FirstOrDefault();
        }

        public ICollection<enterprises> teste()
        {
            return DbSet.ToList();
        }

    }
}
